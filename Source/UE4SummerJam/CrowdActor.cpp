// Fill out your copyright notice in the Description page of Project Settings.


#include "CrowdActor.h"

// Sets default values
ACrowdActor::ACrowdActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ACrowdActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ACrowdActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

FGenericTeamId ACrowdActor::GetGenericTeamId() const
{
	return TeamId;
}

ETeamAttitude::Type ACrowdActor::GetTeamAttitudeTowards(const AActor& Other) const
{

	if (const AActor * OtherPawn = Cast<AActor>(&Other)) {

		if (const IGenericTeamAgentInterface * TeamAgent = Cast<IGenericTeamAgentInterface>(OtherPawn))
		{
			if (TeamAgent->GetGenericTeamId() == TeamId)
			{
				return ETeamAttitude::Friendly;
			}
			else if (TeamAgent->GetGenericTeamId() == NeutralTeamID)
			{
				return ETeamAttitude::Neutral;
			}
			else
			{
				return ETeamAttitude::Hostile;
			}
		}
	}

	return ETeamAttitude::Neutral;
}