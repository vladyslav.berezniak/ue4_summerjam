// Fill out your copyright notice in the Description page of Project Settings.


#include "CrowdCharacter.h"

// Sets default values
ACrowdCharacter::ACrowdCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ACrowdCharacter::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ACrowdCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ACrowdCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}


FGenericTeamId ACrowdCharacter::GetGenericTeamId() const
{
	return TeamId;
}

ETeamAttitude::Type ACrowdCharacter::GetTeamAttitudeTowards(const AActor& Other) const
{

	if (const AActor * OtherPawn = Cast<AActor>(&Other)) {

		if (const IGenericTeamAgentInterface * TeamAgent = Cast<IGenericTeamAgentInterface>(OtherPawn))
		{
			if (TeamAgent->GetGenericTeamId() == TeamId)
			{
				return ETeamAttitude::Friendly;
			}
			else if (TeamAgent->GetGenericTeamId() == NeutralTeamID)
			{
				return ETeamAttitude::Neutral;
			}
			else
			{
				return ETeamAttitude::Hostile;
			}
		}
	}

	return ETeamAttitude::Neutral;
}