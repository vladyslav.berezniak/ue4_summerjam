// Fill out your copyright notice in the Description page of Project Settings.


#include "CrowdPawn.h"

// Sets default values
ACrowdPawn::ACrowdPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
}

// Called when the game starts or when spawned
void ACrowdPawn::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ACrowdPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ACrowdPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

FGenericTeamId ACrowdPawn::GetGenericTeamId() const
{
	return TeamId;
}

ETeamAttitude::Type ACrowdPawn::GetTeamAttitudeTowards(const AActor& Other) const
{

	if (const AActor * OtherPawn = Cast<AActor>(&Other)) {

		if (const IGenericTeamAgentInterface * TeamAgent = Cast<IGenericTeamAgentInterface>(OtherPawn))
		{
			if (TeamAgent->GetGenericTeamId() == TeamId)
			{
				return ETeamAttitude::Friendly;
			}
			else if (TeamAgent->GetGenericTeamId() == NeutralTeamID)
			{
				return ETeamAttitude::Neutral;
			}
			else
			{
				return ETeamAttitude::Hostile;
			}
		}
	}

	return ETeamAttitude::Neutral;
}