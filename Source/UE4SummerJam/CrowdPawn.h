// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "GenericTeamAgentInterface.h"
#include "CrowdPawn.generated.h"

UCLASS()
class UE4SUMMERJAM_API ACrowdPawn : public APawn, public IGenericTeamAgentInterface
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ACrowdPawn();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Team")
		FGenericTeamId TeamId;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Team")
		int NeutralTeamID = 255;

	ETeamAttitude::Type GetTeamAttitudeTowards(const AActor& Other) const override;
private:


	virtual FGenericTeamId GetGenericTeamId() const override;
};
