// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "GenericTeamAgentInterface.h"
#include "PawnCharacter.generated.h"

UCLASS()
class UE4SUMMERJAM_API APawnCharacter : public APawn, public IGenericTeamAgentInterface
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APawnCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Team")
		FGenericTeamId Team_Id;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Team")
		int NeutralTeam_ID = 255;

	ETeamAttitude::Type GetTeamAttitudeTowards(const AActor& Other) const override;

	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class	UFloatingPawnMovement* PawnMovement;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class	UAIPerceptionComponent* AIPerceptionComp;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class	UAISenseConfig_Sight* SightConfig;

private:

	virtual FGenericTeamId GetGenericTeamId() const override;

};
